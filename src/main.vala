int main (string[] args) {
	typeof (Gestures.SwipeStack).ensure ();

	var app = new Gtk.Application ("org.gnome.Gestures", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var screen = Gdk.Screen.get_default ();
		var provider = new Gtk.CssProvider ();
		provider.load_from_resource ("/org/gnome/Gestures/style.css");
		Gtk.StyleContext.add_provider_for_screen (screen, provider, 600);

		var win = app.active_window;
		if (win == null) {
			win = new Gestures.Window (app);
		}
		win.present ();
	});

	return app.run (args);
}
