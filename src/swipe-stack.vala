public class Gestures.SwipeStack : Gtk.Stack {
	private const double[] SHADOW_GRADIENT_OFFSET = { 0, 0.03125, 0.0625, 0.0938, 0.125, 0.1875, 0.25, 0.375, 0.4375, 0.5, 0.5625, 0.625, 0.6875, 0.75, 0.875, 1.0 };
	private const double[] SHADOW_GRADIENT_ALPHA = { 1, 0.99, 0.98, 0.95, 0.92, 0.82, 0.71, 0.46, 0.35, 0.25, 0.17, 0.11, 0.07, 0.04, 0.01, 0.0 };
	private const double SHADOW_WIDTH = 81;
	private const double SHADOW_OPACITY = 0.06;
	private const double DIMMING_OPACITY = 0.12;
	private const double OVERSHOOT_FRICTION = 5;

	private double progress;
	private SwipeTracker.Direction? direction;
	private bool overshoot;

	// TODO: implement other transitions
	public override bool draw (Cairo.Context cr) {
		if (progress == 0)
			return base.draw (cr);

		double offset = (direction == SwipeTracker.Direction.BACK) ? progress : -progress;
		var over = (transition_type == Gtk.StackTransitionType.OVER_LEFT_RIGHT || transition_type == Gtk.StackTransitionType.OVER_UP_DOWN);
		var vert = (transition_type == Gtk.StackTransitionType.OVER_UP_DOWN || transition_type == Gtk.StackTransitionType.SLIDE_UP_DOWN);

		if (over && direction != SwipeTracker.Direction.BACK)
			offset++;

		var width = get_allocated_width ();
		var height = get_allocated_height ();

		var offset_px = Math.round (offset * (vert ? height : width));

		cr.save ();

		cr.rectangle (0, 0, width, height);
		cr.clip ();

		var top_child = get_destination_child ();
		var bottom_child = visible_child;

		if (over && direction == SwipeTracker.Direction.BACK) {
			top_child = visible_child;
			bottom_child = get_destination_child ();
		}

		switch (transition_type) {
		case Gtk.StackTransitionType.SLIDE_LEFT_RIGHT:
			cr.translate (offset_px, 0);
			cr.rectangle (0, 0, width, height);
			break;
		case Gtk.StackTransitionType.SLIDE_UP_DOWN:
			cr.translate (0, offset_px);
			cr.rectangle (0, 0, width, height);
			break;
		case Gtk.StackTransitionType.OVER_LEFT_RIGHT:
			cr.rectangle (0, 0, offset_px, height);
			break;
		case Gtk.StackTransitionType.OVER_UP_DOWN:
			cr.rectangle (0, 0, width, offset_px);
			break;
		}

		cr.clip ();
		propagate_draw (bottom_child, cr);

		if (over) {
			cr.set_operator (Cairo.Operator.ATOP);
			cr.set_source_rgba (0, 0, 0, DIMMING_OPACITY * (1 - offset));
			if (vert)
				cr.rectangle (0, 0, width, offset_px);
			else
				cr.rectangle (0, 0, offset_px, height);
			cr.fill ();
		}

		cr.restore ();

		cr.save ();
		cr.rectangle (0, 0, width, height);
		cr.clip ();

		switch (transition_type) {
		case Gtk.StackTransitionType.SLIDE_LEFT_RIGHT:
			cr.translate (-offset_px / progress.abs(), 0);
			break;
		case Gtk.StackTransitionType.SLIDE_UP_DOWN:
			cr.translate (0, -offset_px / progress.abs());
			break;
		case Gtk.StackTransitionType.OVER_LEFT_RIGHT:
			cr.translate (offset_px, 0);
			break;
		case Gtk.StackTransitionType.OVER_UP_DOWN:
			cr.translate (0, offset_px);
			break;
		}

		if (over) {
			double remainingSwipeDistance = (1 - offset) * width;
			double shadowOpacity = SHADOW_OPACITY;
			if (remainingSwipeDistance < SHADOW_WIDTH)
				shadowOpacity = (remainingSwipeDistance / SHADOW_WIDTH) * SHADOW_OPACITY;

			var shadow_pattern = new Cairo.Pattern.linear (0, 0, vert ? 0 : -SHADOW_WIDTH, vert ? -SHADOW_WIDTH : 0);
			for (int i = 0; i < 16; i++) {
				var _offset = SHADOW_GRADIENT_OFFSET[i];
				var alpha = SHADOW_GRADIENT_ALPHA[i] * shadowOpacity;
				shadow_pattern.add_color_stop_rgba (_offset, 0, 0, 0, alpha);
			}

			cr.save ();
			cr.set_operator (Cairo.Operator.ATOP);

			cr.set_line_width (1);
			cr.set_source_rgba (0, 0, 0, 0.25);
			if (vert) {
				cr.move_to (0, -0.5);
				cr.line_to (width, -0.5);
			} else {
				cr.move_to (-0.5, 0);
				cr.line_to (-0.5, height);
			}
			cr.stroke ();

			if (vert)
				cr.rectangle (0, -SHADOW_WIDTH, width, SHADOW_WIDTH);
			else
				cr.rectangle (-SHADOW_WIDTH, 0, SHADOW_WIDTH, height);
			cr.set_source (shadow_pattern);
			cr.fill ();
			cr.restore ();
		}

		propagate_draw (top_child, cr);

		cr.restore ();

		return true;
	}

	private Gtk.Widget get_previous_child () {
		var children = get_children ();

		unowned List<weak Gtk.Widget> link = children.find (visible_child);
		if (link.prev != null)
			return link.prev.data;

		return children.last ().data;
	}

	private Gtk.Widget get_next_child () {
		var children = get_children ();

		unowned List<weak Gtk.Widget> link = children.find (visible_child);
		if (link.next != null)
			return link.next.data;

		return children.first ().data;
	}

	private Gtk.Widget get_destination_child () {
		if (direction == null)
			return visible_child;

		if (direction == SwipeTracker.Direction.BACK)
			return get_previous_child ();

		if (direction == SwipeTracker.Direction.FORWARD)
			return get_next_child ();

		return visible_child;
	}

	private void allocate_child_size (Gtk.Widget widget) {
		Gtk.Allocation child_allocation = { x: 0, y: 0 };

		int min, nat;
		widget.get_preferred_width (out min, out nat);

		child_allocation.width = int.max (min, get_allocated_width ());

		widget.get_preferred_height_for_width (child_allocation.width, out min, out nat);
		child_allocation.height = int.max (min, get_allocated_height ());

		widget.size_allocate (child_allocation);
	}

	public bool can_go_in_direction (SwipeTracker.Direction direction) {
		if (direction == SwipeTracker.Direction.BACK)
			return get_visible_child () != get_children ().first ().data;

		if (direction == SwipeTracker.Direction.FORWARD)
			return get_visible_child () != get_children ().last ().data;

		return true;
	}

	public void begin (SwipeTracker.Direction direction) {
		this.direction = direction;
		overshoot = !can_go_in_direction (direction);

		get_destination_child ().set_child_visible (true);

		allocate_child_size (get_destination_child ());
	}

	public void update (double progress) {
		this.progress = progress.abs ();

		if (overshoot)
			this.progress = Math.log10 (1 + progress.abs () / OVERSHOOT_FRICTION);

		queue_draw ();
	}

	public void end () {
		get_destination_child ().set_child_visible (false);

		// Avoid double animation
		var transition = transition_type;
		transition_type = Gtk.StackTransitionType.NONE;
		visible_child = get_destination_child ();
		transition_type = transition;

		overshoot = false;
		progress = 0;
		direction = null;

		queue_draw ();
	}

	public void cancel () {
		get_destination_child ().set_child_visible (false);

		progress = 0;
		direction = null;

		queue_draw ();
	}
}
