[GtkTemplate (ui = "/org/gnome/Gestures/window.ui")]
public class Gestures.Window : Gtk.ApplicationWindow {
	[GtkChild]
	private SwipeStack right_stack;
	[GtkChild]
	private Gtk.Button previous_btn;
	[GtkChild]
	private Gtk.Button next_btn;
	[GtkChild]
	private Gtk.Box box;
	[GtkChild]
	private Gtk.RadioButton vert_radio;

	private SwipeTracker tracker;

	public Window (Gtk.Application app) {
		Object (application: app);
	}

	construct {
		right_stack.add_events (Gdk.EventMask.ALL_EVENTS_MASK);

		right_stack.notify["visible-child"].connect (update_buttons);
		vert_radio.toggled.connect (update_orientation);
		update_buttons ();
		update_orientation ();
	}

	private void set_btn_icon_name (Gtk.Button button, string name) {
		var image = button.get_children ().first ().data as Gtk.Image;
		image.icon_name = name;
	}

	private void update_orientation () {
		bool vert = vert_radio.active;

		if (tracker != null) {
			tracker.begin.disconnect (right_stack.begin);
			tracker.end.disconnect(right_stack.end);
			tracker.cancel.disconnect(right_stack.cancel);
			tracker.update.disconnect(right_stack.update);
			tracker = null;
		}

		tracker = new SwipeTracker (right_stack, vert ? Gtk.Orientation.VERTICAL : Gtk.Orientation.HORIZONTAL);

		tracker.begin.connect(right_stack.begin);
		tracker.end.connect(right_stack.end);
		tracker.cancel.connect(right_stack.cancel);
		tracker.update.connect(right_stack.update);

		update_buttons ();

		if (vert) {
			right_stack.transition_type = Gtk.StackTransitionType.OVER_UP_DOWN;
			set_btn_icon_name (previous_btn, "go-up-symbolic");
			set_btn_icon_name (next_btn, "go-down-symbolic");
			box.orientation = Gtk.Orientation.VERTICAL;
			previous_btn.halign = Gtk.Align.CENTER;
			previous_btn.valign = Gtk.Align.START;
			next_btn.halign = Gtk.Align.CENTER;
			next_btn.valign = Gtk.Align.END;
		} else {
			right_stack.transition_type = Gtk.StackTransitionType.OVER_LEFT_RIGHT;
			set_btn_icon_name (previous_btn, "go-previous-symbolic");
			set_btn_icon_name (next_btn, "go-next-symbolic");
			box.orientation = Gtk.Orientation.HORIZONTAL;
			previous_btn.halign = Gtk.Align.START;
			previous_btn.valign = Gtk.Align.CENTER;
			next_btn.halign = Gtk.Align.END;
			next_btn.valign = Gtk.Align.CENTER;
		}
	}

	private void update_buttons () {
		tracker.can_swipe_back = right_stack.can_go_in_direction (SwipeTracker.Direction.BACK);
		tracker.can_swipe_forward = right_stack.can_go_in_direction (SwipeTracker.Direction.FORWARD);

		previous_btn.sensitive = tracker.can_swipe_back;
		next_btn.sensitive = tracker.can_swipe_forward;
	}

	[GtkCallback]
	private void on_previous_clicked () {
		tracker.animate_back (300);
	}

	[GtkCallback]
	private void on_next_clicked () {
		tracker.animate_forward (300);
	}
}
